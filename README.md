How to convert JSON data into a Python object
=============================================

http://stackoverflow.com/questions/6578986/how-to-convert-json-data-into-a-python-object


u can do it in one line, using namedtuple and object_hook:

    import json
    from collections import namedtuple

    data = '{"name": "John Smith", "hometown": {"name": "New York", "id": 123}}'

    # Parse JSON into an object with attributes corresponding to dict keys.
    x = json.loads(data, object_hook=lambda d: namedtuple('X', d.keys())(*d.values()))
    print x.name, x.hometown.name, x.hometown.id

or, to reuse this easily:

    def _json_object_hook(d): return namedtuple('X', d.keys())(*d.values())
    def json2obj(data): return json.loads(data, object_hook=_json_object_hook)

    x = json2obj(data)

If you want it to handle keys that aren't good attribute names, check out namedtuple's rename parameter.

Logging trick
=============
A trick I like is using repeatable -v and -q to select the logging level. The logging levels are actually integers with a 10 increment (DEBUG is 10, CRITICAL is 50). It's very easy with argparse's count action or click's count=True:

    parser.add_argument('-v', '--verbose', action='count', default=0)
    parser.add_argument('-q', '--quiet', action='count', default=0)

    logging_level = logging.WARN + 10*args.quiet - 10*args.verbose

    # script -vv -> DEBUG
    # script -v -> INFO
    # script -> WARNING
    # script -q -> ERROR
    # script -qq -> CRITICAL
    # script -qqq -> no logging at all
