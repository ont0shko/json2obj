
import json
from collections import namedtuple

def from_str(data, name="X"):
    return json.loads(data, object_hook=lambda d: namedtuple(name, d.keys())(*d.values()))

def from_file(filename, name="X"):
    data = ""
    with open(filename) as f:
        data = f.read()

    return from_str(data, name)

if __name__ == '__main__':
    data = {"delta":4, "points":[1,2,3,4,5]}
    s = json.dumps(data)
    o = from_str(s, name='wfm')
    print o.delta, o.points, o.__class__
